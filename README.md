# Modificações

O primeiro commit no repositório foi de uma aplicação com servidor UDP que realizava operações aritméticas inseridas pelo cliente e retornava apenas o resultado. O segundo commit modificou esse código para que o servidor transformasse as letras da mensagem em letras maiúsculas, e o terceiro commit aplicava espaços após cada caracter da mensagem inserida pelo cliente.
