import java.net.*;
import java.io.*;

public class UDPServer {
    public static void main(String[] args) throws Exception {
        DatagramSocket serverSocket = new DatagramSocket(6789);
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];

        while (true) {
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);
            String sentence = new String(receivePacket.getData()).trim();
            InetAddress IPAddress = receivePacket.getAddress();
            int port = receivePacket.getPort();
            String separatedSentence = separateLetters(sentence);
            sendData = separatedSentence.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
            serverSocket.send(sendPacket);
        }
    }

    private static String separateLetters(String sentence) {
        StringBuilder separatedSentence = new StringBuilder();
        for (int i = 0; i < sentence.length(); i++) {
            separatedSentence.append(sentence.charAt(i)).append(" ");
        }
        return separatedSentence.toString();
    }
}